package gtmHpStorage

import (
	"context"
	"io"
	"net/http"

	"gitlab.com/johnrichter/tracing-go"
	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
)

var (
	GTMHTTPClient = http.DefaultClient
)

func TraceGtmWebRequests(service string) {
	GTMHTTPClient = httptrace.WrapClient(
		&http.Client{},
		httptrace.RTWithServiceName(service),
		httptrace.RTWithResourceNamer(gtmWebsiteRTResourceNamer),
	)
}

func gtmWebsiteRTResourceNamer(r *http.Request) string {
	return r.URL.Path
}

func DoGTMWebsite(ctx context.Context, method, url string, body io.Reader) (*http.Response, error) {
	return tracing.Do(ctx, GTMHTTPClient, method, url, body)
}
