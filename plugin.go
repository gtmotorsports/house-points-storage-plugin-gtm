package gtmHpStorage

import (
	"context"

	"github.com/rs/zerolog/log"
	hpevent "gitlab.com/johnrichter/house-points/event"
)

const (
	PluginID = "gtm.web"
)

type GtmWebStorage struct {
	submissionURL string
}

func NewGtmWebStorage(submissionURL string) *GtmWebStorage {
	return &GtmWebStorage{submissionURL: submissionURL}
}

func (s *GtmWebStorage) PluginID() string {
	return PluginID
}

func (s *GtmWebStorage) AwardPoints(
	ctx context.Context,
	hpe *hpevent.HousePointsEvent,
	ape *hpevent.AwardPointsEvent,
) error {
	switch ape.Action {
	case hpevent.EventActionAdd:
		return s.addPoints(
			ctx,
			ape.Amount,
			ape.Beneficiary.ID,
			ape.Explanation,
			ape.Benefactor.ID,
			ape.Conversation.ID,
			hpe.ID,
		)
	case hpevent.EventActionRemove:
		return s.removePoints(
			ctx,
			ape.Amount,
			ape.Beneficiary.ID,
			ape.Explanation,
			ape.Benefactor.ID,
			ape.Conversation.ID,
			hpe.ID,
		)
	}
	log.Debug().Msgf("Unknown AwardPoints event action: %s", ape.Action)
	return nil // Returning an error here will trigger an event retry. We don't want that
}

func (s *GtmWebStorage) LinkShared(
	ctx context.Context,
	hpe *hpevent.HousePointsEvent,
	lse *hpevent.LinkSharedEvent,
) error {
	return s.sendUsedGTMWebsiteBehavior(ctx, lse.User.ID, lse.Conversation.ID, hpe.ID, lse.Urls)
}

func (s *GtmWebStorage) Reaction(
	ctx context.Context,
	hpe *hpevent.HousePointsEvent,
	re *hpevent.ReactionEvent,
) error {
	return s.sendReactionBehavior(
		ctx,
		re.Benefactor.ID,
		re.Action,
		re.Reaction,
		re.Beneficiary.ID,
		re.Conversation.ID,
		hpe.ID,
	)
}
