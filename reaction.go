package gtmHpStorage

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/rs/zerolog/log"
	hp "gitlab.com/johnrichter/house-points"
	"gitlab.com/johnrichter/logging-go"
)

func (s *GtmWebStorage) sendReactionBehavior(
	ctx context.Context,
	user,
	action,
	reaction,
	reacted_to_user,
	channel,
	eventID string,
) error {
	cLog := log.With().
		Str(hp.LogSubmissionMethodKey, hp.SubmissionMethodBehavior).
		Str(hp.LogSubmissionTypeKey, hp.SubmissionTypeSlackReaction).
		Str(hp.LogSubmissionOriginatorSlackUserIDKey, user).
		Str(hp.LogSubmissionReceiverSlackUserIDKey, reacted_to_user).
		Str(hp.LogSubmissionActionKey, action).
		Str(logging.SlackReactionNameKey, reaction).
		Str(logging.SlackChannelIDKey, channel).
		Logger()
	seURL, err := url.Parse(s.submissionURL)
	if err != nil {
		cLog.Error().Err(err).Msg("Malformed House Points submission URL")
		return err
	}
	q := seURL.Query()
	q.Set("award_id", eventID)
	q.Set("award_method", hp.SubmissionMethodBehavior)
	q.Set("award_type", "reaction") // Expected by the GTM website. Can't update to const var
	q.Set("award_action", action)
	q.Set("award_to", reacted_to_user)
	q.Set("award_reaction", reaction)
	q.Set("award_channel", channel)
	q.Set("award_reason", fmt.Sprintf(`{%s} Elicited a :%s: reaction from someone`, channel, reaction))
	q.Set("award_from", user)

	seURL.RawQuery = q.Encode()
	resp, err := DoGTMWebsite(ctx, http.MethodGet, seURL.String(), nil)
	if err != nil || resp.StatusCode != http.StatusOK {
		cLog.Error().Err(err).Msg("Unable to send behavior points to the Submission URL")
		return err
	}
	cLog.Info().Msg("Submitted behavior action")
	return nil
}
