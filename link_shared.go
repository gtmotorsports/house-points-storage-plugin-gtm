package gtmHpStorage

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	hp "gitlab.com/johnrichter/house-points"
)

func (s *GtmWebStorage) sendUsedGTMWebsiteBehavior(
	ctx context.Context,
	user,
	channel,
	eventID string,
	urls []string,
) error {
	cLog := log.With().
		Str(hp.LogSubmissionMethodKey, hp.SubmissionMethodBehavior).
		Str(hp.LogSubmissionTypeKey, hp.SubmissionTypeSharedURL).
		Str(hp.LogSubmissionOriginatorSlackUserIDKey, user).
		Str(hp.LogSubmissionReceiverSlackUserIDKey, user).
		Logger()
	seURL, err := url.Parse(s.submissionURL)
	if err != nil {
		cLog.Error().Err(err).Msg("Malformed House Points submission URL")
		return err
	}
	count := len(urls)
	if count > 0 {
		q := seURL.Query()
		q.Set("award_id", eventID)
		q.Set("award_method", hp.SubmissionMethodBehavior)
		q.Set("award_to", user)
		q.Set("award_channel", channel)
		q.Set("award_type", "used_gtm_website")
		q.Set("award_reason", fmt.Sprintf("Wait? We have a website! %s", strings.Join(urls, " ")))
		for _, u := range urls {
			q.Add("award_url", u)
		}
		seURL.RawQuery = q.Encode()
		resp, err := DoGTMWebsite(ctx, http.MethodGet, seURL.String(), nil)
		if err != nil || resp.StatusCode != http.StatusOK {
			cLog.Error().Err(err).Msg("Unable to send behavior points to the Submission URL")
			return err
		}
	}
	logArr := zerolog.Arr()
	for _, u := range urls {
		logArr = logArr.Str(u)
	}
	cLog.Info().Array(hp.LogSubmissionSharedUrlKey, logArr).Msg("Submitted behavior action")
	return nil
}
