# House Points Plugin

A plugin for the GTM House Points Game which enables points submission to the GTM website.

## Purpose

Meant to be used within the [House Points Worker](https://gitlab.com/johnrichter/house-points-worker).

# Development

Given the polyrepo setup of the various components, you may need to specify local locations for one or more
dependencies. Use the `go.mod.local` file as a guide to build and test this repo locally

In addition, see the [contribution guidelines](CONTRIBUTING.md).
