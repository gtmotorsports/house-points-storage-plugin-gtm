package gtmHpStorage

import (
	"context"
	"net/http"
	"net/url"
	"strconv"

	"github.com/rs/zerolog/log"
	hp "gitlab.com/johnrichter/house-points"
)

func (s *GtmWebStorage) addPoints(
	ctx context.Context,
	amount int,
	user,
	reason,
	originating_user,
	channel,
	eventID string,
) error {
	cLog := log.With().
		Str(hp.LogSubmissionMethodKey, hp.SubmissionMethodDirect).
		Str(hp.LogSubmissionTypeKey, hp.SubmissionTypeDirectAward).
		Str(hp.LogSubmissionActionKey, hp.SubmissionActionAdd).
		Int(hp.LogSubmissionPointCountKey, amount).
		Str(hp.LogSubmissionOriginatorSlackUserIDKey, originating_user).
		Str(hp.LogSubmissionReceiverSlackUserIDKey, user).
		Logger()
	seURL, err := url.Parse(s.submissionURL)
	if err != nil {
		cLog.Error().Err(err).Msg("Malformed House Points submission URL")
		return err
	}
	q := seURL.Query()
	q.Set("award_id", eventID)
	q.Set("award_method", hp.SubmissionMethodDirect)
	q.Set("award_channel", channel)
	q.Set("award_to", user)
	q.Set("award_action", hp.SubmissionActionAdd)
	q.Set("award_type", "Give") // Deprecated and replaced by award_action.
	q.Set("award_value", strconv.Itoa(amount))
	q.Set("award_reason", reason)
	seURL.RawQuery = q.Encode()

	resp, err := DoGTMWebsite(ctx, http.MethodGet, seURL.String(), nil)
	if err != nil || resp.StatusCode != http.StatusOK {
		cLog.Error().Err(err).Msg("Unable to send direct points to the Submission URL")
		return err
	}
	cLog.Info().Msg("Submitted direct award points")
	return nil
}

func (s *GtmWebStorage) removePoints(
	ctx context.Context,
	amount int,
	user,
	reason,
	originating_user,
	channel,
	eventID string,
) error {
	cLog := log.With().
		Str(hp.LogSubmissionMethodKey, hp.SubmissionMethodDirect).
		Str(hp.LogSubmissionTypeKey, hp.SubmissionTypeDirectAward).
		Str(hp.LogSubmissionActionKey, hp.SubmissionActionRemove).
		Int(hp.LogSubmissionPointCountKey, amount).
		Str(hp.LogSubmissionOriginatorSlackUserIDKey, originating_user).
		Str(hp.LogSubmissionReceiverSlackUserIDKey, user).
		Logger()
	seURL, err := url.Parse(s.submissionURL)
	if err != nil {
		log.Error().Err(err).Msg("Malformed House Points submission URL")
		return err
	}
	q := seURL.Query()
	q.Set("award_id", eventID)
	q.Set("award_method", hp.SubmissionMethodDirect)
	q.Set("award_channel", channel)
	q.Set("award_to", user)
	q.Set("award_action", hp.SubmissionActionRemove)
	q.Set("award_type", "Take Away") // Deprecated and replaced by award_action.
	q.Set("award_value", strconv.Itoa(amount))
	q.Set("award_reason", reason)
	seURL.RawQuery = q.Encode()

	resp, err := DoGTMWebsite(ctx, http.MethodGet, seURL.String(), nil)
	if err != nil || resp.StatusCode != http.StatusOK {
		cLog.Error().Err(err).Msg("Unable to send direct points to the Submission URL")
		return err
	}
	cLog.Info().Msg("Submitted direct award points")
	return nil
}
